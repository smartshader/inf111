package tp1;

/**
 * This program calculates the squart root of entered value.
 *
 * @author Imad Jerra
 * @version 2015-09-04
 */
import java.util.Scanner;

public class Sqrt
{
    // initialize constants
    public static final double EPSILON = 0.00001;

    // Initialize Scanner variable
    public static Scanner input = new Scanner(System.in);

    public static void main(String []args){
        // Print welcome message
        System.out.println("This program calculates the square root.");

        // Get a value from user
        System.out.printf("Enter a value: ");
        double a = input.nextDouble();

        // Calculates sqrt following newton method
        double x = a;
        while (x * x - a > EPSILON){
            x = (x/2)+(a/(2*x));
        }

        // Print value
        System.out.printf("The square root of %s is: %1.2f", a, x);
    }
}
