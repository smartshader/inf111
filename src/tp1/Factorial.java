package tp1;

/**
 * This program prompts the user to enter an integer value and prints its factorial.
 *
 * @author Imad Jerra
 * @version 2015-09-04
 */
import java.util.Scanner;

public class Factorial
{
    public static void main(String []args) {
        // initialize Scanner variable
        Scanner input = new Scanner(System.in);

        // Get a value from user
        System.out.printf("Enter integer: ");
        int value = input.nextInt();

        // Variable to store factorial
        int fact = 1;

        // Calculate factorial
        int n = 1;
        while (n <= value){
            fact = fact * n;
            n = n + 1;
        }

        // Print factorial
        System.out.println("Factorial is: " + fact);
    }
}