package tp2;

/**
 * Created by imad on 9/11/15.
 */
import java.util.Scanner;

public class Calculator {
    // Initialize Scanner variable
    public static Scanner input = new Scanner(System.in);

    // minimum limit
    public static final int MIN = 1;

    // maximum limit
    public static final int MAX = 1000;

    // main method
    public static void main(String []args){
        // Print welcome message
        System.out.println("This program calculates the greatest common divisor");
        System.out.println("and the least common multiple of 2 given values a & b");
        System.out.println();

        // Get 2 values
        int a = getInteger("Enter number a: ", MIN, MAX);
        int b = getInteger("Enter number b: ", MIN, MAX);

        // Print result
        System.out.println("The greatest common divisor of " + a + " and " + b + " is: " + pgcd(a, b));
        System.out.println("The least common multiple of " + a + " and " + b + " is: " + ppcm(a, b));
    }

    /**
     * Prompts the user to enter integer
     * @param message message to show for prompt
     * @param min     minimum value limit
     * @param max     maximum value limit
     * @return        double entered by user
     */
    public static int getInteger(String message, int min, int max){
        int value;

        // Prompt the user until the entered value is within min & max
        do {
            System.out.printf(message);
            value = input.nextInt();
        } while (value < min || value > max);

        return value;
    }

    /**
     * Calculate the greatest common divisor of 2 values
     * @param a first value
     * @param b second value
     * @return  the greatest common divisor
     */
    public static int pgcd(int a, int b){
        int pgcd;

        // while a & b are different from 0
        // assign the biggest of the two the value of it minus the other
        while (a != 0 && b != 0){

            if (a > b){
                a = a - b;
            } else {
                b = b - a;
            }

        }

        // if a is 0 then the pgcd is b otherwise it is a
        if (a == 0){
            pgcd = b;
        } else {
            pgcd = a;
        }

        return pgcd;
    }

    /**
     * Calculate the least common multiple of 2 values
     * @param a first value
     * @param b second value
     * @return  the least common multiple
     */
    public static int ppcm(int a, int b){
        return (a / pgcd(a, b)) * b;
    }
}
