package tp2;

/**
 * This program calculates the squart root of entered value.
 *
 * @author Imad Jerra
 * @version 2015-09-11
 */
import java.util.Scanner;

public class Sqrt
{
    // initialize constants
    public static final double EPSILON = 0.00001;

    // Initialize Scanner variable
    public static Scanner input = new Scanner(System.in);

    // main method
    public static void main(String []args){
        // Print welcome message
        System.out.println("This program calculates the square root.");

        // Get a value from user
        double a = getDouble("Enter a value: ");

        // Print value
        System.out.printf("The square root of %s is: %1.2f", a, sqrt(a));
    }

    /**
     * Calculate square root following newton method
     * @param a double to calculate square root of
     * @return  square root of a
     */
    public static double sqrt(double a){
        double x = a;
        while (x * x - a > EPSILON){
            x = (x/2)+(a/(2*x));
        }

        return x;
    }

    /**
     * Prompts the user to enter double
     * @param message message to show for prompt
     * @return        double entered by user
     */
    public static double getDouble(String message){
        System.out.printf(message);
        double value = input.nextDouble();

        return value;
    }
}
