package tp2;

/**
 * This program prompts the user to enter an integer value and prints its factorial.
 *
 * @author Imad Jerra
 * @version 2015-09-11
 */

import java.util.Scanner;

public class Factorial
{
    // initialize Scanner variable
    public static Scanner input = new Scanner(System.in);

    // main method
    public static void main(String []args) {
        // Get a value from user
        int integer = getInteger("Enter integer: ");

        // Print factorial
        System.out.println("Factorial is: " + factorial(integer));
    }

    /**
     * Calculates factorial of given integer
     * @param integer value to calculate factorial of
     * @return        factorial of given integer
     */
    public static int factorial(int integer){
        // Variable to store factorial
        int fact = 1;

        // Calculate factorial
        int n = 1;
        while (n <= integer){
            fact = fact * n;
            n++;
        }

        return fact;
    }

    /**
     * Prompts the user to enter integer
     * @param message message to show for prompt
     * @return        integer entered by user
     */
    public static int getInteger(String message){
        System.out.printf(message);
        int value = input.nextInt();

        return value;
    }
}